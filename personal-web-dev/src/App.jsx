import React from 'react';
import About from "./components/About/About.jsx";
import Contact from "./components/Contact/Contact.jsx";
import Footer from "./components/Footer/Footer.jsx";
import Header from "./components/Header/Header.jsx";
import Portfolio from "./components/Portfolio/Portfolio.jsx";
import Resume from "./components/Resume/Resume.jsx";

function App() {
    return (
        <>
            <Resume />
        </>
    );
}


export default App;
